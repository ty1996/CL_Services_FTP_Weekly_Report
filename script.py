## Python 3.7
## simple_salesforce is a required module that must be installed separately from Python

from ftplib import FTP
from simple_salesforce import Salesforce
import csv
import os
import re

#####

def main():

    ## Salesforce Login

    username = #REDACTED
    password = #REDACTED
    isSandbox = 0
    token = #REDACTED
    instance = #REDACTED

    sf = Salesforce(username=username, password=password, security_token=token, instance_url=instance,sandbox=isSandbox)

    ## User inputs a start and end date to pull the data from

    start_date = input("Input Start Date in the format yyyy-mm-dd.\n")
    end_date = input("Input End Date in the format yyyy-mm-dd.\n")

    ## Script attempts to run the query and will only continue if it was successful.

    try:
        results = sf.query("SELECT First_Stop_City__c, First_Stop_State__c, First_Stop_Zip5__c, Last_Stop_City__c, Last_Stop_State__c, "
            "Last_Stop_Zip5__c, rtms__Customer_Quote_Total__c, FTP_Equipment__c, FTP_Ship_Date__c FROM rtms__Load__c WHERE "
            "FTP_Ship_Date__c > " + start_date + " and FTP_Ship_Date__C < " + end_date + " order by FTP_Ship_Date__c") 
    except:
        print("Error in your date format. Please try again.")
        main()

    ### Parse the query in to lists ###

    value_list = []
    placeholder_list = []
    counter = 0

    ## Iterate over results and place them in to lists.

    for i, record in enumerate(results['records']):

        for key, value in record.items():

            if key == 'attributes':
                continue # Skips the metadeta

            if counter <= 9:
                placeholder_list.append(value)
                counter += 1

                if counter == 9:
                    value_list.append(placeholder_list)
                    placeholder_list = []
                    counter = 0

    header = [

        'First_Stop_City__c',
        'First_Stop_State__c',
        'First_Stop_Zip5__c',
        'Last_Stop_City__c',
        'Last_Stop_State__c',
        'Last_Stop_Zip5__c',
        'rtms__Customer_Quote_Total__c',
        'FTP_Equipment__c',
        'FTP_Ship_Date__c'

    ]

    ## Write headers and data to a CSV

    with open("output.csv", "w") as f: ## Output.csv is the name of the file that will be created. If a file already exists, it will be overwitten.
        writer = csv.writer(f)
        writer.writerow(header)
        for row in value_list:
            writer.writerow(row)

     downloadFile(start_date, end_date) ## Passes user input dates so the next function can use it to name the file

 def downloadFile(start_date, end_date):

     ## FTP Login Information

     ftp = FTP(#REDACTED)
     ftp_user = #REDACTED
     ftp_pw = #REDACTED

     ftp.login(user=ftp_user, passwd=ftp_pw) ## Initiates the FTP connection

     ## Downloads the file

     filename = "pricingdata.txt"
     localfile = open('pricingdata.txt', 'wb')
     ftp.retrbinary('RETR ' + filename, localfile.write, 1024)
     localfile.close()
     os.rename('pricingdata.txt', 'Geexpert Pricing Data ' + start_date + ' - ' + end_date + '.csv') ## Renames the file downloaded from the server

 def uploadFile():

     filename = 'clservices.csv' ## Name of the file that is uplaoded to the server. If there is a file on the server with the same name, it will automatically be overwritten.
     ftp.storbinary('STOR ' + filename, open(filename, 'rb'))
     ftp.quit() ## Closes the FTP connection

#####

if __name__ == '__main__':
    main()